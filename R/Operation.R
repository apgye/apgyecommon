#' @export
checkWarn <- function(v) {
  length(na.omit(v)) && na.omit(v) == FALSE
}


#' @importFrom R6 R6Class
#' @import futile.logger
#' @import lubridate
#' @export
Operation <- R6::R6Class(
  # Set the name for the class
  "Operation",


  # slots
  public = list(
    inputSpec = NULL,
    interval = NULL,
    initialize = function(operationInterval) {
      self$interval <- operationInterval
      classname <- class(self)[1]
      self$inputSpec <- Operation$readSpec(classname)
    },
    process = function(input) {
      do.call(private$processInput, input)
    },

    validateInput = function(input){

    },
    parseInputEntry = function(name, value) {
      specEntry <- self$inputSpec[[name]]
      switch(specEntry$type,
             "integer" = { private$parseInteger(specEntry, value) },
             "data.frame" = { private$parseDataframe(specEntry, value) }
      )
    },
    withinInterval = function(datesToCheck, substractDay = FALSE) {
      if (substractDay) {
        interval_ajust <- self$interval
        interval_ajust$.Data <- interval_ajust$.Data - 86400
        datesToCheck %within% interval_ajust
      } else {
        datesToCheck %within% self$interval
      }
    },
    help = function() { }
  ),
  private = list(
    PATH = NULL,
    parseDataframe = function(specEntry, value) {
      validationResult <- c()
      df <- value
      eq_col_names <- tolower(colnames(df)) == tolower(specEntry$expectedColumns)
      if(any(!eq_col_names)) {
        validationResult <- c(validationResult, paste0("columnas esperadas: ", toString(specEntry$expectedColumns), "\n", "columnas leidas: ", toString(colnames(df)), sep = ""))      }
      eq_col_types <- unlist(lapply(df, class), use.names = F) == specEntry$expectedTypes
      attr(df, "problems") <- validationResult
      df
    },
    parseInteger = function(specEntry, value) {
      validationResult <- c()
      intValue <- NA
      if((is.na(value) || is.null(value) || value == "")) {
        if(is.null(specEntry$optional) || specEntry$optional != TRUE ) {
          validationResult <- c(validationResult, "El valor es requerido")
        }
      } else {
        intValue <- suppressWarnings( if (length(value) == 1) as.integer(value) else NA)
        if(is.na(intValue)) {
          validationResult <- c(validationResult, "El valor no es un número entero válido")

        } else {
          if(!is.null(specEntry$minimum) && specEntry$minimum > intValue) {
              validationResult <- c(validationResult, paste("El valor mínimo es :", specEntry$minimum))
          }
          if(!is.null(specEntry$maximum) && specEntry$maximum < intValue) {
              validationResult <- c(validationResult, paste("El valor máximo es :", specEntry$maximum))
          }
        }
      }
      attr(intValue, "problems") <- validationResult
      intValue

    }
  )

)


Operation$readSpec <- function(classname) {
  where <- getAnywhere(classname)$where
  matching <- grep("package:(.*)", where, value = TRUE)
  if(length(matching)>1) {
    flog.error("multiple packages using the same operation name %s", matching)
    stop("multiple packages using the same operation name")
  }
  package <- substring(matching, 9)[1]
  specFileName <-  paste(classname, "_spec.json", sep='')
  specPath <-  system.file("apgyeOperation", specFileName, package = package)

  if(specPath == "") { ## this is just a hack in order to support operation packages under development
    specPath <-  system.file("inst", "apgyeOperation", specFileName, package = package)
  }

  flog.debug("loading input spec: %s:%s %s ", package, classname, specPath)
  inputSpec <- RJSONIO::fromJSON(specPath, encoding = "UTF-8")
  inputSpec <- if(is.null(names(inputSpec))) inputSpec[[1]] else inputSpec

  inputSpec
}
